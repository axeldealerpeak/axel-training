package data

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"

	"axel-training/vehicle"
)

var vehicles = make([]vehicle.Vehicle, 0)

func Save(Year int, VehicleMake string, Model string, Stocknumber string, Photo []int) {
	newVehicle := vehicle.Vehicle{Year: Year, VehicleMake: VehicleMake, Model: Model, Stocknumber: Stocknumber, Photo: Photo}
	vehicles = append(vehicles, newVehicle)
}

func FindVehicleByStocknumber(Stocknumber string) *vehicle.Vehicle {
	for i := range vehicles {
		if Stocknumber == vehicles[i].Stocknumber {
			return &vehicles[i]
		}
	}
	return nil
}

func PrintAllVehicles() {
	for _, vehicle := range vehicles {
		fmt.Printf("Year: %d, Make: %s, Model: %s, Stocknumber: %s\n", vehicle.Year, vehicle.VehicleMake, vehicle.Model, vehicle.Stocknumber)
	}
}

func ReturnAllVehicles() []vehicle.Vehicle {
	return vehicles
}

func ImportVehicles() {
	files, err := ioutil.ReadDir("photos")
	if err != nil {
		log.Fatal(err)
	}
	for _, file := range files {
		if err != nil {
			log.Fatal(err)
		}
		importVehicle := strings.Split(file.Name(), "_")
		year, _ := strconv.Atoi(importVehicle[0])
		vehiclemake := importVehicle[1]
		model := importVehicle[2]
		jpeg := strings.Split(importVehicle[3], "-")
		stocknumber := jpeg[0]
		photoFile := strings.Split(jpeg[1], ".")
		photoNumber, _ := strconv.Atoi(photoFile[0])
		v := FindVehicleByStocknumber(stocknumber)
		if v != nil {
			v.Photo = append(v.Photo, photoNumber)
		} else {
			photos := []int{photoNumber}
			Save(year, vehiclemake, model, stocknumber, photos)
		}
	}
}
