var app = angular.module('TrainingExercise', [
    'ngRoute'
]);

app.config(['$routeProvider', function($routes){
    $routes.when('/', {
        templateUrl : 'pages/vehicleList.html',
        controller  : 'MainController'
    });
    $routes.when('/:stocknumber', {
        templateUrl : 'pages/vehicle.html',
        controller  : 'VehicleController'
    });
    $routes.otherwise({
        redirectTo  : '/'
    });
}]);

app.controller('MainController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){
    $scope.vehicles = [];

    $http.get("/api/vehicles/")
    .then(function(response){
        $scope.vehicles = response.data;
    }, function(response){
        console.log('There was an error.');
    });
}]);

app.controller('VehicleController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){
    var stocknumber = $routeParams.stocknumber;
    $scope.activePhoto = 0;
    $scope.changeMainPic = function() {
        $scope.activePhoto = this.photo;
    }

    $http.get("/api/vehicles/"+stocknumber)
    .then(function(singleCar){
        $scope.car = singleCar.data;
    }, function(singleCar){
        console.log('There was an error.');
    });
}]);
