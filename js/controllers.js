angular.module('TrainingExercise.controllers', []).
controller('vehiclesController', function($scope, vehiclesService) {
    $scope.nameFilter = null;
    $scope.vehiclesList = [];

    vehiclesService.getVehicles().success(function (data) {
        $scope.vehiclesList = data
    });
});
