angular.module('TrainingExercise.services', []).
    factory('vehiclesService', function($http) {

        var vehicles = {};

        vehicles.getVehicles = function() {
            return $http({
                method: 'JSONP',
                url: 'http://localhost:8080/vehicles/'
            })
        }
    })
