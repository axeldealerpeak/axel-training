package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"

	"axel-training/data"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello world!")
}

func vehiclesHandler(w http.ResponseWriter, r *http.Request) {
	jsonData, err := json.Marshal(data.ReturnAllVehicles())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	fmt.Fprintf(w, string(jsonData))
}

func vehicleHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	stocknumber := vars["stocknumber"]
	jsonData, err := json.Marshal(data.FindVehicleByStocknumber(stocknumber))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	fmt.Fprintf(w, string(jsonData))
}

func main() {
	data.ImportVehicles()
	r := mux.NewRouter()
	r.HandleFunc("/api/", handler)
	r.HandleFunc("/api/vehicles/", vehiclesHandler)
	r.HandleFunc("/api/vehicles/{stocknumber}", vehicleHandler)
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("/home/axel/dpnext/src/axel-training")))

	http.ListenAndServe(":8080", r)
}
