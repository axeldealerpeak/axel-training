package postgres

import (
	"axel-training/data"
	"fmt"
	"log"

	"database/sql"

	_ "github.com/lib/pq"
)

const (
	dbUser     = "axel"
	dbPassword = "password"
	dbName     = "axel"
)

func AddVehiclesToPostgresDB() {
	dbInfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",
		dbUser, dbPassword, dbName)
	db, err := sql.Open("postgres", dbInfo)
	checkErr(err)
	defer db.Close()

	var insertString string = "INSERT INTO vehicles (year, vehiclemake, model, stocknumber) VALUES ($1,$2,$3,$4);"
	individualInsert, err := db.Prepare(insertString)
	checkErr(err)
	for _, vehicle := range data.ReturnAllVehicles() {
		v, err := individualInsert.Exec(vehicle.Year, vehicle.VehicleMake, vehicle.Model, vehicle.Stocknumber)
		if err != nil || v == nil {
			log.Fatal(err)
		}
	}
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
