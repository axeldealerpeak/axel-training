package vehicle

type Vehicle struct {
	Year        int
	VehicleMake string
	Model       string
	Stocknumber string
	Photo       []int
}
