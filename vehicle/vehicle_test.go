package vehicle

import (
  "testing"
)

func TestCreateVehicle(t *testing.T){
  testVehicle := Vehicle{Year: 2001, VehicleMake: "Ford", Model: "Focus", Stocknumber: "A123"}
  if 2001 != testVehicle.Year {
    t.Fail()
  }
}
